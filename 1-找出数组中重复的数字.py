# 用defaultdict也可以，但是没必要，因为约定了数的范围，可以用数组来替代，两次遍历就可以了

class Solution:
    def duplicateInArray(self, nums):
        n,res = len(nums),-1
        st = [0] * n
        for num in nums:
            if num < 0 or num > n - 1:
                return -1
            st[num] += 1
        for idx,val in enumerate(st):
            if val > 1:
                res = idx
                break
        return res 