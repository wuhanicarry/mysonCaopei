class Solution(object):
    def Fibonacci(self, n):
        f1,f2 = 0,1
        if n < 2:
            return n
        while n >= 2:
            bk = f2
            f2 = f1 + f2
            f1 = bk
            n -= 1
        return f2