class Solution(object):
    def isNumber(self, s):
        s = s.strip()
        if not s: return False
        dot,e,sign = False,False,False
        for i in range(len(s)):
            if s[i] == '.':
                if dot:return False
                elif len(s) == 1: return False
                elif i and s[i - 1] in ['+','-'] and i==len(s) - 1:return False
                elif e:return False
                dot = True
            elif s[i] in ['E','e']:
                if e or i == len(s) - 1 or i == 0:return False
                if i and not s[i - 1].isdigit():return False
                e = True
            elif s[i]in ['+','-']:
                if sign:
                    if i and s[i - 1] not in ['E','e']:return False
                else:
                    if not e and i != 0:return False
                    elif e and i == len(s) - 1: return False
                    sign = True
            elif not s[i].isdigit():
                return False
        return True
