# 分情况讨论，有右儿子就是右子树的最左节点。没有的话就往上找，直到当前节点是其父节点的左儿子
class Solution(object):
    def inorderSuccessor(self, q):
        if not q: return None
        if q.right:
            q = q.right
            while q.left:
                q = q.left
            return q
        while q.father and q.father.right == q:
            q = q.father
        if q.father: return q.father
        return None