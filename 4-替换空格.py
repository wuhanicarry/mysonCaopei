# 用c++来写就是一个很经典的双指针

class Solution(object):
    def replaceSpaces(self, s):
        res = ''
        for i in s:
            if i == ' ':
                res += '%20'
            else:
                res += i
        return res