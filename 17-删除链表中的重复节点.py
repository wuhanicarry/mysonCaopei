class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def deleteDuplication(self, head):
        if not head:
            return None
        dummy = ListNode(-1)
        dummy.next = head
        tmp = dummy
        while tmp.next:
            ne = tmp.next
            while ne and tmp.next.val == ne.val:
                ne = ne.next
            if tmp.next.next == ne:
                tmp = tmp.next
            else: tmp.next = ne
        return dummy.next