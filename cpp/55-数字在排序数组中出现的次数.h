class Solution {
public:
    int getNumberOfK(vector<int>& nums , int k) {
        int n = nums.size();
        if(n == 0) return 0;
        int l = 0,r = nums.size() - 1;
        while(l < r){
            int mid = l + r + 1>> 1;
            if(nums[mid] <= k) l = mid;
            else r = mid - 1;
        }
        int rhs = r;
        if(nums[rhs] != k) return 0;
        l = 0,r = nums.size() -1;
        while(l < r){
            int mid = l + r >> 1;
            if(nums[mid] >= k) r = mid;
            else l = mid + 1;
        }
        int lhs = l;
        return rhs - lhs + 1;
    }
};