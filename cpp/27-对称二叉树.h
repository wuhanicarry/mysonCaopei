class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        return !root || dfs(root->left,root->right);
    }
    bool dfs(TreeNode* lhs,TreeNode* rhs){
        if(!lhs || !rhs) return !lhs && !rhs;
        return lhs->val == rhs->val && dfs(lhs->left,rhs->right) && dfs(lhs->right,rhs->left);
    }
};