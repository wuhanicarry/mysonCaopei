class Solution {
public:
    string printMinNumber(vector<int>& nums) {
        vector<string> vec;
        for(auto num : nums){
            vec.push_back(to_string(num));
        }
        sort(vec.begin(),vec.end(),[](auto& lhs,auto &rhs){
           return lhs + rhs < rhs + lhs; 
        });
        string s;
        for(auto num : vec){
            s += num;
        }
        return s;
    }
};