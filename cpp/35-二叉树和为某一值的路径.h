class Solution {
public:
    vector<vector<int>> res;
    vector<vector<int>> findPath(TreeNode* root, int sum) {
        if(!root) return res;
        vector<int> path;
        dfs(root,sum,path);
        return res;
    }

    void dfs(TreeNode* root,int sum,vector<int> &path){
        if(!root) return;
        path.push_back(root->val);
        sum -= root->val;
        if(!root->left && !root->right && !sum) res.push_back(path);
        dfs(root->left,sum,path);
        dfs(root->right,sum,path);
        path.pop_back();
        sum += root->val;
    }
};
