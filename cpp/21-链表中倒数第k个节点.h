class Solution {
public:
    ListNode* findKthToTail(ListNode* head, int k) {
        int n = 0;
        auto t = head;
        while(t){
            n ++;
            t = t->next;
        }
        if(k > n) return NULL;
        n = n - k,t = head;;
        while(n--){
            t = t -> next;
        }
        return t;
    }
};
