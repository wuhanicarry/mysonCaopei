class Solution {
public:
    typedef long long LL;
    int digitAtIndex(int n) {
        // i代表当前的str的n位是出现在几位数中的
        // num是当前位数有多少个数
        // base是当前数字的起始
        LL i = 1,num = 9,base = 1;
        while(n > i * num){
            n -= num * i;
            i++;
            base *= 10;
            num *= 10;
        }
        // 一个上取整，找到当前数字
        int number = base + (n + i - 1) / i  - 1;
        // 当前数字的第几位
        int r = n % i ? n % i : i;
        // 找到这个数字
        for(int j = 0;j < i - r;j++) number /= 10;
        return number % 10;
    }
};
