class Solution {
public:
    vector<int> multiply(const vector<int>& A) {
        int n = A.size();
        vector<int> res(n);
        vector<int> lhs(n,1),rhs(n,1);
        for(int i = 1;i < n;i++){
            lhs[i] = lhs[i - 1] * A[i - 1];
        }
        for(int i = n - 2;i >= 0;i--){
            rhs[i] = A[i + 1] * rhs[i + 1];
        }
        for(int i = 0;i < n;i++){
            res[i] = lhs[i] * rhs[i];
        }
        return res;
    }
};