class Solution {
public:
    unordered_set<ListNode*> mp;
    ListNode *findFirstCommonNode(ListNode *headA, ListNode *headB) {
        while(headA || headB){
            if(headA){
                if(!mp.count(headA)) mp.insert(headA);
                else return headA;
                headA = headA->next;
            }
            if(headB){
                if(!mp.count(headB)) mp.insert(headB);
                else return headB; 
                headB = headB->next;
            }
        }
        return NULL;
    }
};