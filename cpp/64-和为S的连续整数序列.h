class Solution {
public:
    vector<vector<int> > findContinuousSequence(int sum) {
        vector<vector<int>> res;
        for(int i = 1,j = 1,s = 1;i <= sum;i++){
            while(s < sum) j++,s += j;
            if(s == sum && j > i){
                vector<int> line;
                for(int x = i;x <= j;x++) line.push_back(x);
                res.push_back(line);
            }
            s -= i;
        }
        return res;
    }
};