// 递归的解法可能过不掉，这里考虑动态规划

class Solution {
public:
    vector<int> numberOfDice(int n) {
        int dp[n + 1][6 * n + 1];
        memset(dp,0,sizeof dp);
        dp[0][0] = 1;
        for(int i = 1;i <= n;i++){
            for(int j = 1;j <= 6 * i;j++){
                for(int k = 1;k <= 6;k++){
                    if(j >= k){
                        dp[i][j] += dp[i - 1][j - k];
                    }
                }
            }
        }
        vector<int> res;
        for(int i = n;i <= 6 * n;i++) res.push_back(dp[n][i]);
        return res;
    }
};
