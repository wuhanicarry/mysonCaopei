class Solution {
public:
    int strToInt(string str) {
        int minux = 0;
        long long number = 0;
        int k = 0;
        while(k < str.size() && str[k] == ' ') k++;
        if(str[k] == '+') k++;
        else if(str[k] == '-') k++,minux = 1;

        while (k < str.size() && str[k] >= '0' && str[k] <= '9') {
            number = number * 10 + str[k] - '0';
            k ++ ;
        }
        if(minux) number *= -1;
        if (number > INT_MAX) number = INT_MAX;
        if (number < INT_MIN) number = INT_MIN;
        return (int)number;
    }
};