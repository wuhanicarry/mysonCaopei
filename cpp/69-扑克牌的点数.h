class Solution {
public:
    bool isContinuous( vector<int> nums ) {
        sort(nums.begin(),nums.end());
        for(int i = 1;i < nums.size();i++){
            if(nums[i] && nums[i] == nums[i - 1]) return false;
        }
        for(auto x : nums){
            if(x) return nums.back() - x <= 4;
        }
    }
};
