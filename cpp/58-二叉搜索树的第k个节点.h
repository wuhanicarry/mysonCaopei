class Solution {
public:
    vector<TreeNode*> rec;
    TreeNode* kthNode(TreeNode* root, int k) {
        dfs(root);
        return rec[k - 1];
    }
    void dfs(TreeNode* root){
        if(!root) return;
        dfs(root->left);
        rec.push_back(root);
        dfs(root->right);
    }
};