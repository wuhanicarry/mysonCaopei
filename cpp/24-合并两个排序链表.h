class Solution {
public:
    ListNode* merge(ListNode* l1, ListNode* l2) {
        auto dummy = new ListNode(-1);
        auto tmp = dummy,h1 = l1,h2 = l2;
        while(h1 && h2){
            if(h1->val > h2->val){
                tmp->next = h2;
                tmp = tmp->next;
                h2 = h2->next;
            }else{
                tmp->next = h1;
                tmp = tmp->next;
                h1 = h1->next;
            }
        }
        tmp->next = h1?h1:h2;
        return dummy->next;
    }
};