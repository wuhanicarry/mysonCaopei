class Solution {
public:
    vector<int> findNumsAppearOnce(vector<int>& nums) {
        int diff = 0;
        for(auto x : nums) diff ^= x;
        int k = 0;
        while(!(diff >> k & 1)) k++;
        int num = 0;
        for(auto x : nums){
            if(x >> k & 1) {
                num ^= x;
            }
        }
        return vector<int> {diff ^ num,num};
    }
};