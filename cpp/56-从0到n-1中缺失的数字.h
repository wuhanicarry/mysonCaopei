class Solution {
public:
    int getMissingNumber(vector<int>& nums) {
        sort(nums.begin(),nums.end());
        nums.push_back(-1);
        for(int i = 0;i < nums.size();i++){
            if(nums[i] != i) return i;
        }
        return 0;
    }
};