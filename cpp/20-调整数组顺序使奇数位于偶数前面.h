class Solution {
public:
    void reOrderArray(vector<int> &a) {
        int l = 0,r = a.size() - 1;
        int n = a.size();
        while(l < r){
            while(l < n && a[l] & 1) l++;
            while(r >= 0 && (a[r] & 1) == 0) r--;
            if(l < r) swap(a[l++],a[r--]);
        }
    }
};
