class Solution {
public:
    vector<int> findNumbersWithSum(vector<int>& nums, int target) {
        unordered_map<int,int> mp;
        for(int i = 0;i < nums.size();i++){
            if(mp.count(nums[i])) return vector<int> {target - nums[i],nums[i]};
            mp[target - nums[i]] = i;
        }
        return vector<int> {0,0};
    }
};