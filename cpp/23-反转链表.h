class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* p = NULL,*q = head;
        while(q){
            auto t = q->next;
            q->next = p;
            p = q;
            q = t;
        }
        return p;
    }
};