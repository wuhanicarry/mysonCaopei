class Solution {
public:
    ListNode *copyRandomList(ListNode *head) {
        auto h1 = head;
        while(h1){
            auto node = new ListNode(h1->val);
            auto ne = h1->next;
            h1->next = node;
            node->next = ne;
            h1 = ne;
        }
        h1 = head;
        while(h1){
            if(h1->random){
                h1->next->random = h1->random->next;
            }
            h1 = h1->next->next;
        }
        auto dummy = new ListNode(-1);
        auto h2 = dummy;
        h1 = head;
        while(h1){
            h2->next = h1->next;
            h2 = h2->next;
            h1->next = h1->next->next;
            h1 = h1->next;
        }
        return dummy->next;
    }
};
