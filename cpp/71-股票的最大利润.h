class Solution {
public:
    int maxDiff(vector<int>& nums) {
        int res = 0;
        for(int i = 0,j = INT_MAX;i < nums.size();i++){
            if(j < nums[i]){
                res = max(res,nums[i] - j);
            }
            j = min(nums[i],j);
        }
        return res;
    }
};