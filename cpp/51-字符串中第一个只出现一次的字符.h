class Solution {
public:
    char firstNotRepeatingChar(string s) {
        unordered_map<char,int> mp;
        for(auto ch:s) mp[ch]++;
        for(auto ch:s){
            if(mp[ch] == 1) return ch;
        }
        return '#';
    }
};