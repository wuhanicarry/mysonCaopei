class Solution {
public:
    ListNode *entryNodeOfLoop(ListNode *head) {
        if(!head) return NULL;
        auto fast = head,slow = head;
        while(fast && slow){
            slow = slow->next;
            fast = fast->next;
            if(fast) fast = fast->next;
            else return NULL;
            if(fast == slow){
                slow = head;
                while(fast != slow){
                    fast = fast -> next;
                    slow = slow -> next;
                }
                return slow;
            }
        }
        return NULL;
    }
};