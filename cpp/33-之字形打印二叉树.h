class Solution {
public:
    vector<vector<int>> printFromTopToBottom(TreeNode* root) {
        vector<vector<int>> res;
        if(!root) return res;
        queue<TreeNode*> q;
        q.push(root);
        bool d = false;
        while(q.size()){
            int n = q.size();
            vector<int> cur;
            for(int i = 0;i < n;i++){
                auto t = q.front();
                q.pop();
                cur.push_back(t->val);
                if(t->left) q.push(t->left);
                if(t->right) q.push(t->right);
            }
            if(d) reverse(cur.begin(),cur.end());
            res.push_back(cur);
            d = !d;
        }
        return res;
    }
};
