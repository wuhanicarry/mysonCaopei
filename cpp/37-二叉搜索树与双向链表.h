class Solution {
public:
    TreeNode* pre = NULL;
    TreeNode* convert(TreeNode* root) {
        if(!root) return root;
        dfs(root);
        while(root->left) root = root->left;
        return root;
    }
    void dfs(TreeNode* root){
        if(!root) return;
        dfs(root->left);
        root->left = pre;
        if(pre) pre->right = root;
        pre = root;
        dfs(root->right);
    }
};