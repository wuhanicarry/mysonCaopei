class MinStack {
public:
    //push更小的到min,每个弹出的时候也顺势弹出min
    MinStack() {

    }

    void push(int x) {
        stk.push(x);
        if(mini.size()) mini.push(min(mini.top(),x));
        else mini.push(x);
    }

    void pop() {
        stk.pop();
        mini.pop();
    }

    int top() {
        return stk.top();
    }

    int getMin() {
        return mini.top();
    }
private:
    stack<int> stk,mini;
};
