class Solution {
public:
    int getTranslationCount(string s) {
        int n = s.size();
        int dp[n + 1];
        memset(dp,0,sizeof dp);
        dp[0] = 1;
        for(int i = 1;i <= n;i++){
            dp[i] = dp[i - 1];
            if(i > 1){
                int t = s[i - 1] - '0' + (s[i - 2] - '0') * 10;
                if(t >= 10 && t <= 25) dp[i] += dp[i - 2];
            }
        }
        return dp[n];
    }
};s