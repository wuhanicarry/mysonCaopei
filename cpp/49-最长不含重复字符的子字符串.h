class Solution {
public:
    int longestSubstringWithoutDuplication(string s) {
        int st[26],res = 0;
        memset(st,0,sizeof st);
        for(int i = 0,j = 0;j < s.size();j++){
            if(++st[s[j] - 'a'] > 1){
                while(i < j){
                    st[s[i] - 'a']--;
                    i++;
                    if(st[s[j] - 'a'] == 1) break;
                }
            }
            res = max(res,j - i + 1);
        }
        return res;
    }
};