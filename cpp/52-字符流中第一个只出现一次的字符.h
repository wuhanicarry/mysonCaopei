class Solution{
public:
    queue<char> q;
    unordered_map<char,int> cnt;
    //Insert one char from stringstream
    void insert(char ch){
        q.push(ch);
        cnt[ch] ++;
        if(cnt[ch] > 1){
            while(q.size() && cnt[q.front()] > 1){
                q.pop();
            }
        }
    }
    //return the first appearence once char in current stringstream
    char firstAppearingOnce(){
        if(q.size()) return q.front();
        return '#';
    }
};