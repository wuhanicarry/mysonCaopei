class Solution {
public:
    int dfs(TreeNode* root){
        if(!root) return 0;
        int res = 1;
        res += max(dfs(root->left),dfs(root->right));
        return res;
    }

    int treeDepth(TreeNode* root) {
        if(!root) return 0;
        int res = 1;
        res += max(dfs(root->left),dfs(root->right));
        return res;    
    }
};