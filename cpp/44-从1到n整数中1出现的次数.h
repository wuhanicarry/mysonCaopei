class Solution {
public:
    int numberOf1Between1AndN_Solution(int n) {
        vector<int> nums;
        while(n){
            nums.push_back(n % 10);
            n /= 10;
        }
        int res = 0;
        for(int i = nums.size() - 1;i >= 0;i--){
            int lhs = 0,rhs = 0,t = 1;
            for(int j = nums.size() - 1;j > i;j--) lhs = lhs * 10 + nums[j];
            for(int j = i - 1;j >= 0;j--) rhs = rhs * 10 + nums[j],t *= 10;

            res += t * lhs;
            if(nums[i] == 1) res += rhs + 1;
            if(nums[i] > 1) res += t;
        }
        return res;
    }
};