class Solution {
public:
    bool isPopOrder(vector<int> a1,vector<int> a2) {
        if(a1.size() != a2.size()) return false;
        int it = 0,n = a1.size();
        stack<int> stk;
        for(int i = 0;i < n;i++){
            stk.push(a1[i]);
            while(stk.size() && stk.top() == a2[it]){
                it ++;
                stk.pop();
            }
        }
        return stk.empty();
    }
};