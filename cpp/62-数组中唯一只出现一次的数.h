class Solution {
public:
    int findNumberAppearingOnce(vector<int>& nums) {
        int res = 0;
        for(int i = 31;i >= 0;i--){
            int cnt = 0;
            for(auto x : nums){
                if(x >> i & 1) cnt++;
            }
            if(cnt % 3 == 1) res |= 1 << i;
        }
        return res;
    }
};