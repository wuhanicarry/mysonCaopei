class Solution {
public:
    bool hasSubtree(TreeNode* h1, TreeNode* h2) {
        if(!h1 || !h2) return false;
        if(issame(h1,h2)) return true;
        return hasSubtree(h1->left,h2) || hasSubtree(h1->right,h2);
    }
    bool issame(TreeNode* h1,TreeNode* h2){
        if(!h2) return true;
        if(!h1 || h2->val != h1->val) return false;
        return issame(h1->left,h2->left) && issame(h1->right,h2->right);
    }
};
