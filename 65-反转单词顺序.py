class Solution(object):
    def reverseWords(self, s):
        q = list(s.split())
        q.reverse()
        return ' '.join(q)