# 满足某种二段性，可以二分。去除冗余之后进行二分找到二段性的划分点

class Solution:
    def findMin(self, nums):
        if not nums:
            return -1
        n = len(nums) - 1
        while n > 0 and nums[n] == nums[0]: n -= 1
        if nums[n] >= nums[0]:
            return nums[0]
        l,r = 0,len(nums) - 1
        while l < r:
            mid = (l + r) >> 1
            if nums[mid] < nums[0]: r = mid
            else: l = mid + 1
        return nums[l]