# 有3，-3，不然就凑3
class Solution(object):
    def maxProductAfterCutting(self,n):
        if n <= 3: return 1 * (n - 1)
        res = 1
        if n % 3 == 1:
            n -= 4
            res *= 4
        if n % 3 == 2:
            n -= 2
            res *= 2
        while n:
            n -= 3
            res *= 3
        return res