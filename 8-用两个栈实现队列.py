# 经典无意义
class MyQueue(object):
    def __init__(self):
        self.stk = []
        self.bk = []

    def push(self, x):
        self.stk.append(x)

    def pop(self):
        while self.stk:
            self.bk.append(self.stk.pop())
        tmp = self.bk.pop()
        while self.bk:
            self.stk.append(self.bk.pop())
        return tmp

    def peek(self):
        while self.stk:
            self.bk.append(self.stk.pop())
        tmp = self.bk[-1]
        while self.bk:
            self.stk.append(self.bk.pop())
        return tmp

    def empty(self):
        if self.stk:
            return False
        return True