# 简单链表遍历

class Solution(object):
    def printListReversingly(self, head):
        tmp = head
        res = []
        while tmp:
            res.append(tmp.val)
            tmp = tmp.next
        res.reverse()
        return res