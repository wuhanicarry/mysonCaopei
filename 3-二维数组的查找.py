# 从第一行最右边开始找，因为确定是第一行最小，且每行从左到右是递增的，每列从上到下是递增的

class Solution(object):
    def searchArray(self, array, target):
        if not array or not array[0]:
            return False
        i,j = 0,len(array[0]) - 1
        while i < len(array) and j >= 0:
            t = array[i][j]
            if t == target: return True
            if t > target: j -= 1
            else: i += 1
        return False