# 快速幂
class Solution(object):
    def Power(self, base, exponent):
        ng,res = 1,1
        if exponent < 0:
            ng = -1
        k = abs(exponent)
        while k != 0:
            if k & 1 == 1:
                res *= base
            base *= base
            k >>= 1
        if ng < 0:
            res = 1 / res
        return res