class Solution(object):
    def leftRotateString(self, s, n):
        s = list(s)
        l = s[0:n]
        r = s[n:]
        return ''.join(r + l)